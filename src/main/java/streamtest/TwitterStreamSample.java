package streamtest;
import java.io.PrintWriter;

import org.clapper.util.io.IOExceptionExt;
import org.clapper.util.io.RollingFileWriter;
import org.clapper.util.io.RollingFileWriter.Compression;
import twitter4j.*;

public final class TwitterStreamSample {
    /**
     * Main entry of this application.
     *
     * @param args arguments doesn't take effect with this example
     * @throws TwitterException when Twitter service or network is unavailable
     * @throws IOExceptionExt 
     */
    public static void main(String[] args) throws TwitterException, IOExceptionExt {
      final PrintWriter w = new RollingFileWriter("tweet${n}.out", 100000000, 10000, Compression.DONT_COMPRESS_BACKUPS);

      TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
        RawStreamListener listener = new RawStreamListener() {
            @Override
            public void onMessage(String msg) {
              w.println(msg);
            }

            @Override
            public void onException(Exception arg0) {
            }
        };
        twitterStream.addListener(listener);
        twitterStream.sample();
    }
}
