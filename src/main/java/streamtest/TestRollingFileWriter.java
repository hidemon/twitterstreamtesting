package streamtest;

import java.io.PrintWriter;


import org.clapper.util.io.IOExceptionExt;
import org.clapper.util.io.RollingFileWriter;

public class TestRollingFileWriter {
  public static void main(String[] args) throws IOExceptionExt {
    try (PrintWriter w = new RollingFileWriter("test${n}.out")) {
      w.println("test");
    }
  }  
}
